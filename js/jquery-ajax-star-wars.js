"use strict";

//create a local variable and assign my access token
// var apiToken = "8meweP_3PouHHkCMRPea";

//create a local variable and assign the api url
var api = "https://swapi.dev/api/people";

//make a GET request to the api variable, console.log the data
$.get(api, {

}).done(function (data) {
    const characters = data.results;
    // Loop through each of the characters and display their information on the table
    for (let i = 0; i < characters.length; i++) {
        const character = characters[i];

        // The films and the homeworld information are located in another URL
        // Insert each ajax request to those URLS in an array
        const ajaxRequests = [];
        let home = '';
        // Insert homeworld ajax request into array
        ajaxRequests.push($.get(character.homeworld).done(function(data) {
            home = data.name;
        }));
        const films = [];
        // For each film, insert an ajax request into array
        for (let j = 0; j < character.films.length; j++) {
            const film = character.films[j];
            ajaxRequests.push($.get(film).done(function(data) {
                films.push(data.title);
            }));
        }

        // Wait until all of those ajax requests are done before creating the row for the character
        $.when(...ajaxRequests).then(function(objects) {
            // Create a string out of all the films that character starred in
            const allFilms = films.join(', ');
            const characterHTML = `
         <tr>
            <th scope="row">${character.name}</th>
            <td>${character.height}</td>
            <td>${character.mass}</td>
            <td>${character.birth_year}</td>
            <td>${character.gender}</td>
            <td>${home}</td>
            <td>${allFilms}</td>
        </tr>
        `;
            // Insert th character row into the body section of table
            $('#insertCharacters').append(characterHTML);
        })

    }
});